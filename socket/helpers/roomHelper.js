import { getUsersInfo } from './usersHelper.js';

export const addUserToRoom = (roomsMap, roomName, userId) => {
  const currentUsersInRoom = roomsMap.get(roomName).connectedUsers;
  currentUsersInRoom.push(userId);
};

export const removeUserFromRoom = (roomsMap, roomName, userId) => {
  const currentRoom = roomsMap.get(roomName);
  const currentUsersInRoom = currentRoom.connectedUsers;
  const currentWinnersInRoom = currentRoom.winners;
  let index = currentUsersInRoom.indexOf(userId);
  currentUsersInRoom.splice(index, 1);
  if (currentWinnersInRoom.length) {
    index = currentWinnersInRoom.indexOf(userId);
    currentWinnersInRoom.splice(index, 1);
  }
};

export const updateRoomsAfterLeave = (usersMap, roomsMap, roomName, userId) => {
  removeUserFromRoom(roomsMap, roomName, userId);

  const currentUser = usersMap.get(userId);
  const currentUserInRoom = roomsMap.get(roomName).connectedUsers;
  usersMap.set(userId, { ...currentUser, readyStatus: false, progress: 0 });

  const usersList = getUsersInfo(usersMap, currentUserInRoom);
  if (!currentUserInRoom.length) {
    roomsMap.delete(roomName);
  }
  return usersList;
};
