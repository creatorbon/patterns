import * as config from '../config';
import { usersData, roomsData } from '../index';
import { getUsersInfo } from './usersHelper';

const getId = (length) => Math.floor(Math.random() * length);

class Factory {
  constructor() {
    this.createPhrase = function (type) {
      let phrase;
      if (type === 'START_GAME') {
        phrase = new GameStartPhrase();
      } else if (type === 'JOKE') {
        phrase = new JokePhrase();
      } else if (type === 'INTRODUCE') {
        phrase = new IntroducePhrase();
      } else if (type === 'END_RACE') {
        phrase = new EndRacePhrase();
      } else if (type === 'END_GAME') {
        phrase = new EndGamePhrase();
      } else if (type === 'TEMPORARY_RESULT') {
        phrase = new TemporaryResultPhrase();
      } else if (type === 'ALMOST_END') {
        phrase = new AlmostEndPhrase();
      }
      phrase.say = function (users) {
        return `${this.phrase(users)}`;
      };
      return phrase;
    };
  }
}

class GameStartPhrase {
  constructor() {
    this.phrase = (users) => {
      const sentence = [
        `Поприветствуем наших участников. Сегодня учавствуют: ${users.map((user) => user.name).join(', ')}`,
        `Сегодня с нами ${users.map((user) => user.name).join(', ')}. Борьба будет не шуточной`,
        `${users.map((user) => user.name).join(', ')} решили сегодня выяснить, кто из них быстрее`,
      ];
      return sentence[getId(sentence.length)];
    };
  }
}

class JokePhrase {
  constructor() {
    const jokes = [
      'Понедельник - это удобный повод чего- нибудь понеделать.',
      'Больше всего тишина сближает тех, кто тайком делит деньги.',
      'Надо ли отвечать "Ок" на сообщение "Не пиши мне больше!"?',
      'А вы знали, что после выхода на пенсию уже можно свистеть в доме?',
      'Судя по всему, камбалу ловят кувалдой.',
      'А вы знали, что виноград взрывается в микроволновой печи?',
      'Первоначально кока-кола была зеленой.',
      'Арахис используется в производстве динамита.',
      'У человека меньше мускулов, чем у гусеницы.',
      'Дельфины спят с одним открытым глазом.',
    ];
    this.phrase = (users) => {
      return jokes[getId(jokes.length)];
    };
  }
}

class AlmostEndPhrase {
  constructor() {
    this.phrase = (users) => {
      const sentence = [
        `${users.map((user) => user.name).join(', ')} практически завершил гонку! До финиша осталось совсем немного!`,
        `Ребята поторопитесь. ${users.map((user) => user.name).join(', ')} практически закончил`,
        `Еще чуток и ${users.map((user) => user.name).join(', ')} на финише`,
        `Вы там играете? ${users.map((user) => user.name).join(', ')} уже почти на финише`,
      ];
      return sentence[getId(sentence.length)];
    };
  }
}

class IntroducePhrase {
  constructor() {
    const keyboard = ['Razer', 'HyperX', 'Asus', 'HP', 'A4Tech', 'Apple', 'Logitech', 'SteelSeries'];
    this.phrase = (users) => {
      let count = 1;
      let template = '';
      users.forEach((user) => {
        template += `Игрок под номером №${count} ${user.name} на клавиатуре ${keyboard[getId(keyboard.length)]} \n`;
        count++;
        return template;
      });
      return template;
    };
  }
}

class TemporaryResultPhrase {
  constructor() {
    this.phrase = (users) => {
      let template = '';
      const currentUsers = users;
      template += `На данный момент лидирует ${
        currentUsers[0].name
      } и он смог пройти уже ${currentUsers[0].progress.toFixed()}% пути. \n`;

      if (currentUsers.length > 1) {
        currentUsers.splice(0, 1);
        let count = 2;

        currentUsers.map((user) => {
          if (currentUsers.length === count - 1) {
            template += `И на последнем месте ${user.name}, который преодолел ${user.progress.toFixed()}% пути \n`;
          } else {
            template += `${user.name} преодолел ${user.progress.toFixed()}% пути \n`;
            count++;
          }
        });
      }
      return template;
    };
  }
}

class EndRacePhrase {
  constructor() {
    this.phrase = (users) => {
      const sentence = [
        `${users.map((user) => user.name).join()} пересек финишную линию`,
        `${users.map((user) => user.name).join()} закончил гонку`,
        `A ${users.map((user) => user.name).join()} уже финишировал`,
        `Финишную линию пересекает ${users.map((user) => user.name).join()}`,
      ];
      return sentence[getId(sentence.length)];
    };
  }
}

class EndGamePhrase {
  constructor() {
    this.phrase = (users) => {
      let count = 1;
      let template = '';
      users.forEach((user) => {
        template += `${user.name} закончил игру на №${count} месте \n`;
        count++;
        return template;
      });
      return template;
    };
  }
}

const factory = new Factory();
const startGame = factory.createPhrase('START_GAME');
const introducePlayers = factory.createPhrase('INTRODUCE');
const joke = factory.createPhrase('JOKE');
const endGame = factory.createPhrase('END_GAME');
const endRace = factory.createPhrase('END_RACE');
const temporary = factory.createPhrase('TEMPORARY_RESULT');
const almostEnd = factory.createPhrase('ALMOST_END');
let counter = 0;
let interval;

export const initCommentator = (usersList, roomName, io) => {
  const timer = config.SECONDS_TIMER_BEFORE_START_GAME / 2;
  io.to(roomName).emit('COMMENTATOR_PHRASE', startGame.say(usersList));
  const endRaceLog = [];
  const almostEndLog = [];

  const commentLogic = () => {
    counter > config.SECONDS_FOR_GAME + timer ? (counter = 0) : counter++;
    const currentRoom = roomsData.get(roomName);
    const currentUserInRoom = currentRoom?.connectedUsers || [];
    const winners = currentRoom?.winners || [];
    const usersList = getUsersInfo(usersData, currentUserInRoom);
    const winnersList = getUsersInfo(usersData, winners);
    const usersAlmostEnd = usersList.filter((user) => user.progress >= 60);
    const userEndRace = usersList.filter((user) => user.progress === 100);

    if (currentUserInRoom.length === winners.length || counter === config.SECONDS_FOR_GAME + timer) {
      // End game comment
      clearInterval(interval);
      endRaceLog.length = 0;
      almostEndLog.length = 0;
      const winners = winnersList.length >= currentUserInRoom.length ? winnersList : usersList;
      io.to(roomName).emit('COMMENTATOR_PHRASE', endGame.say(winners));
    } else if (counter % 30 === 0 && counter !== 0) {
      // Comment result every 30 sec
      usersList.sort((user, nextUser) => nextUser.progress - user.progress);
      io.to(roomName).emit('COMMENTATOR_PHRASE', temporary.say(usersList));
    } else if (counter % 10 === 0 && counter !== 0) {
      // Random comments
      io.to(roomName).emit('COMMENTATOR_PHRASE', joke.say(usersList));
    } else if (userEndRace.length > endRaceLog.length) {
      // User end race comment
      userEndRace.forEach((user) => {
        if (endRaceLog.indexOf(user.id) === -1) {
          io.to(roomName).emit('COMMENTATOR_PHRASE', endRace.say(Array(user)));
          endRaceLog.push(user.id);
        }
      });
    } else if (usersAlmostEnd.length > almostEndLog.length) {
      // User almost end race comment
      usersAlmostEnd.forEach((user) => {
        if (almostEndLog.indexOf(user.id) === -1) {
          io.to(roomName).emit('COMMENTATOR_PHRASE', almostEnd.say(Array(user)));
          almostEndLog.push(user.id);
        }
      });
    }
  };

  setTimeout(() => {
    io.to(roomName).emit('COMMENTATOR_PHRASE', introducePlayers.say(usersList));
    interval = setInterval(commentLogic, 1000);
  }, timer * 1000);
};
