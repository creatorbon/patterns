import {
  createRoomPreviw,
  joinRoom,
  refreshRoom,
  updateUsersInRoom,
  leaveRoom,
  changeReadyStatus,
} from './helpers/roomHelper.mjs';
import { addClass, createElement, removeClass } from './helpers/domHelper.mjs';
import { gameStartTimer } from './helpers/gameHelper.mjs';

const username = sessionStorage.getItem('username');
const roomsContainer = document.getElementById('rooms-container');
const addRoomButtom = document.getElementById('add-room');
const leaveRoomButtom = document.getElementById('leave-room');
const readyButtom = document.getElementById('ready');
const refreshButtom = document.getElementById('refresh');

if (!username) {
  window.location.replace('/login');
}

const socket = io('', { query: { username } });

socket.on('ADD_USER_ERROR', () => {
  alert('Already have user with this name');
  sessionStorage.clear();
  window.location.replace('/login');
});

socket.on('ADD_ROOM_ERROR', () => {
  alert('Already have room with this name');
});

socket.on('UPDATE_ROOMS', (rooms) => {
  const validRooms = rooms.filter(
    (room) => room.gameStart !== true && room.maxUsersForRoom > room.connectedUsers.length,
  );
  const updatedRooms = validRooms.map((room) => createRoomPreviw(room, socket));
  roomsContainer.innerHTML = '';
  roomsContainer.append(...updatedRooms);
});

socket.on('CONNECT_TO_ROOM', joinRoom);

socket.on('UPDATE_USERS', (userList) => updateUsersInRoom(userList));

socket.on('UPDATE_ROOM', () => refreshRoom());

socket.on('GAME_IS_READY', ({ timeToGameStart, timeToGameEnd, textId }) => {
  addClass(leaveRoomButtom, 'display-none');
  gameStartTimer(timeToGameStart, timeToGameEnd, textId, socket);
});

socket.on('UPADATE_USER_STATUS', (status) => changeReadyStatus(status));

socket.on('GAME_OVER', (winners) => {
  const roomName = document.getElementById('room-name').innerText.trim();
  const container = createElement({ tagName: 'ol' });
  clearInterval(window.myInterval);
  winners.map((user) => (container.innerHTML += `<li>${user.name}</li>`));
  removeClass(refreshButtom, 'display-none');
  refreshButtom.addEventListener('click', () => socket.emit('REFRESH_GAME', roomName));
});

addRoomButtom.addEventListener('click', () => {
  const roomName = prompt('Please enter room name') || '';
  socket.emit('CREATE_ROOM', roomName);
});

leaveRoomButtom.addEventListener('click', () => {
  const roomName = document.getElementById('room-name').innerText.trim();
  socket.emit('LEAVE_ROOM', roomName);
  leaveRoom();
});

readyButtom.addEventListener('click', () => {
  const roomName = document.getElementById('room-name').innerText.trim();
  socket.emit('CHANGE_READY_STATUS', { roomName, userId: socket.id });
});

socket.on('COMMENTATOR_PHRASE', (phrase) => {
  document.getElementById('commentator__text').innerText = phrase;
});
